<?php



/**
 * main function if you want to run it yourself manually
 *
 * Could use given test cases:
 *
    main("1101"); // 13 remainder 1
    main("1110"); // 14 remainter 2
    main("1111"); // 15 remainder 0
    main("1001010"); // 74 remainder 2

 */
function main($input) {

    $states = ["S0", "S1", "S2"];
    $inputAlphabet = ["1", "0"];
    $initialState = "S0";
    $validFinalStates = ["S0", "S1", "S2"];
    $transitions = [
        "S0" => [
            "0" => "S0",
            "1" => "S1",
        ],
        "S1" => [
            "0" => "S2",
            "1" => "S0",
        ],
        "S2" => [
            "0" => "S1",
            "1" => "S2",
        ]
    ];

    $stateConversion = [
        "S0" => "0",
        "S1" => "1",
        "S2" => "2",
    ];

    $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions, $stateConversion);

    for ($i = 0; $i < strlen($input); $i++) {
        $stateMachine->transition($input[$i]);
    }

    echo $stateMachine->finalStateOutput() . "\n";
}


class StateMachine {

    private $states;
    private $inputAlphabet;
    private $initialState;
    private $validFinalStates;
    private $transitions;
    private $stateConversionTable;

    public $currentState;


    /**
     * StateMachine constructor.
     * @param $states - array of strings representing each state name. E.g. ["S0", "S1", "S2"];
     * @param $inputAlphabet - array of string representing each allowed input character. E.g. ["1", "0"]
     * @param $initialState - initial state name
     * @param $validFinalStates - array of strings representing the state names that are valid ending states
     *
     * @param $transitions - a map consisting of state names and input alphabets representing the transition logic. E.g.
     *   [
     *       "S0" => [
     *           "0" => "S0",
     *           "1" => "S1",
     *       ],
     *       "S1" => [
     *           "0" => "S2",
     *           "1" => "S0",
     *       ],
     *       "S2" => [
     *           "0" => "S1",
     *           "1" => "S2",
     *       ]
     *   ];
     * @param null $stateConversionTable - optional argument if we want to output the representation of each state
     *             at the end of our transitions. E.g.
     *
     *     $stateConversion = [
     *         "S0" => "0",
     *         "S1" => "1",
     *         "S2" => "2",
     *     ];
     * @throws InvalidArgumentException
     */
    public function __construct($states, $inputAlphabet, $initialState, $validFinalStates, $transitions, $stateConversionTable = null) {

        $validator = new StateMachineValidator();
        $validator->validate($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);

        if ($validator->hasErrors()) {
            throw new InvalidArgumentException(implode(', ', $validator->getErrors()));
        }

        $this->states = $states;
        $this->inputAlphabet = $inputAlphabet;
        $this->initialState = $initialState;
        $this->validFinalStates = $validFinalStates;
        $this->transitions = $transitions;
        $this->stateConversionTable = $stateConversionTable;


        $this->currentState = $initialState;
    }


    /**
     *
     * Transitions states based on the given transition instructions
     *
     * @param $input
     * @throws InvalidArgumentException
     */
    public function transition($input) {

        if (!$this->isValidInput($input) || !isset($this->transitions[$this->currentState][$input])) {
            throw new InvalidArgumentException();
        }

        $this->currentState = $this->transitions[$this->currentState][$input];
    }

    /**
     * Outputs the final state. If the final state is not in the list of valid final states, it will just warn you with an echo.
     * If a state conversion table was given, it will output the value based on it. If not, it will just output the
     * final state name.
     *
     */
    public function finalStateOutput() {

        if (!in_array($this->currentState, $this->validFinalStates)) {
            echo "Invalid final state\n";
        }

        if (isset($this->stateConversionTable[$this->currentState])) {
            echo $this->stateConversionTable[$this->currentState];
        } else {
            echo $this->currentState;
        }

    }

    /**
     * @param $input
     * @return bool - whether the input bit is in the alphabet
     */
    private function isValidInput($input) {
        return in_array($input, $this->inputAlphabet);
    }
}

/**
 *
 * A set of functions to help validate the state machine inputs
 *
 */
class StateMachineValidator {


    private $errors = [];

    /**
     * @param $states
     * @param $inputAlphabet
     * @param $initialState
     * @param $validFinalStates
     * @param $transitions
     */
    public function validate($states, $inputAlphabet, $initialState, $validFinalStates, $transitions) {

        // check initial state is in states
        $this->validateInitialState($states, $initialState);

        // check every valid final state is in initial state
        $this->validateFinalStates($states, $validFinalStates);

        $this->validateTransitionStruct($states, $inputAlphabet, $transitions);

    }

    public function hasErrors() {
        return count($this->errors) > 0;
    }

    public function getErrors() {
        return $this->errors;
    }

    /**
     * Checks the initial state is in the list of states given
     * @param $states
     * @param $initialState
     */
    private function validateInitialState($states, $initialState) {
        if (!in_array($initialState, $states)) {
            $this->errors[] = "Initial state not in given states";
        }
    }

    /**
     * Creates a associative array from a regular array of strings for constatnt time lookup 
     * E.g. ['val'] turns into ['val' => true]
     *
     * @param $arr an array of strings
     */
    private function getAsAssoc($arr) {
        $assoc = [];
        foreach ($arr as $val) {
            $assoc[$val] = true;
        }
        return $assoc;
    }

    /**
     * Checks if the given final state is in the list of states given
     *
     * @param $states
     * @param $validFinalStates
     */
    private function validateFinalStates($states, $validFinalStates) {
        $statesAssoc = $this->getAsAssoc($states);;

        foreach ($validFinalStates as $validFinalState) {
            if (!isset($statesAssoc[$validFinalState])) {
                $this->errors[] = "Final state not preset in valid states";
                return;
            }
        }
    }

    /*
     *
     * Checks that the transition structure is properly constructed:
     *   1. every state key must be in the list of given $states
     *   2. every state transition input must be in the given input alphabet
     *   3. every transition state must be in the list of given states
     *
     *   $transitions = [
     *       "S0" => [
     *           "0" => "S0",
     *           "1" => "S1",
     *       ],
     *       "S1" => [
     *           "0" => "S2",
     *           "1" => "S0",
     *       ],
     *       "S2" => [
     *           "0" => "S1",
     *           "1" => "S2",
     *       ]
     *   ];
     */
    private function validateTransitionStruct($states, $inputAlphabet, $transitions) {

        $statesAssoc = $this->getAsAssoc($states);
        $inputsAssoc = $this->getAsAssoc($inputAlphabet);

        foreach ($transitions as $stateName => $stateInputs) {

            // check every transition states is in states
            if (!isset($statesAssoc[$stateName])) {
                $this->errors[] = "Invalid transition struct - state name $stateName is not valid";
            }

            foreach ($stateInputs as $input => $newState) {

                // check every transition alphabet is in input alphabet
                if (!isset($inputsAssoc[$input])) {
                    $this->errors[] = "Invalid transition struct - input $input is not in the valid alphabet";
                }

                // check every transition => alphabet => new transition is in $states
                if (!isset($statesAssoc[$newState])) {
                    $this->errors[] = "Invalid transition struct - state transition $stateName is not valid";
                }
            }
        }
    }


}



