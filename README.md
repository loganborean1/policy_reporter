# Policy Reporter Technical Exercise

All the non-testing code was put in index.php for simplicity. There is a main function at the beginning of index.php if you would like to run things manually to test.


## Sample Usage (Mod 3)


```php
function main($input) {                                                                               
                                                                                                      
    $states = ["S0", "S1", "S2"];                                                                     
    $inputAlphabet = ["1", "0"];                                                                      
    $initialState = "S0";
    $validFinalStates = ["S0", "S1", "S2"];                                                           
    $transitions = [                                                                                  
        "S0" => [
            "0" => "S0",                                                                              
            "1" => "S1",                                                                              
        ],
        "S1" => [
            "0" => "S2",                                                                              
            "1" => "S0",                                                                              
        ],
        "S2" => [
            "0" => "S1",                                                                              
            "1" => "S2",                                                                              
        ]                                                                                             
    ];                                                                                                
    
    $stateConversion = [                                                                              
        "S0" => "0",                                                                                  
        "S1" => "1",                                                                                  
        "S2" => "2",                                                                                  
    ];                                                                                                
    
    $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions, $stateConversion);
                                                                                                      
    for ($i = 0; $i < strlen($input); $i++) {                                                         
        $stateMachine->transition($input[$i]);                                                        
    }                                                                                                 
                                                                                                      
                                                                                                      
    echo $stateMachine->finalStateOutput() . "\n";                                                    
                                                                                                      
}  
```

## Running Tests

A very simple test runner was made to run a handful of tests in tests.php. You can just run with php and it will execute the tests:

```bash
php test.php
```
