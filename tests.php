<?php
require_once('./index.php');


$testRunner = new TestRunner();
$testRunner->runTests();

class TestRunner {

    private $passed = 0;
    private $failed = [];


    public function runTests() {
        $this->passed = 0;
        $this->failed = [];


        $this->testModThree();
        $this->testModFive();

        $this->testInvalidInputAlphabet();

        $this->testInvalidInitialState();
        $this->testInvalidFinalStates();

        $this->testInvalidTransitionStructState();
        $this->testInvalidTransitionStructAlphabet();
        $this->testInvalidTransitionStructAlphabetTransitionState();


        echo "PASSED: $this->passed\n";
        echo "FAILED: " . count($this->failed) . "\n" .  
            implode("\n", $this->failed) . "\n";

    }

    public function testModThree() {

        $states = ["S0", "S1", "S2"];
        $inputAlphabet = ["1", "0"];
        $initialState = "S0";
        $validFinalStates = ["S0", "S1", "S2"];
        $transitions = [
            "S0" => [ "0" => "S0", "1" => "S1", ],
            "S1" => [ "0" => "S2", "1" => "S0", ],
            "S2" => [ "0" => "S1", "1" => "S2", ]
        ];

        $input = "1101";

        $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);

        for ($i = 0; $i < strlen($input); $i++) {
            $stateMachine->transition($input[$i]);
        }

        $stateMachine->currentState === "S1" ? $this->passed() : $this->failed(__FUNCTION__);
    }

    public function testModFive() {

        $states = ["S0", "S1", "S2", "S3", "S4"];
        $inputAlphabet = ["1", "0"];
        $initialState = "S0";
        $validFinalStates = ["S0", "S1", "S2", "S4"];
        $transitions = [
            "S0" => [ "0" => "S0", "1" => "S1", ],
            "S1" => [ "0" => "S2", "1" => "S3", ],
            "S2" => [ "0" => "S4", "1" => "S0", ],
            "S3" => [ "0" => "S1", "1" => "S2", ],
            "S4" => [ "0" => "S3", "1" => "S4", ]
        ];

        $input = "00101110110";

        $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);

        for ($i = 0; $i < strlen($input); $i++) {
            $stateMachine->transition($input[$i]);
        }

        $stateMachine->currentState === "S4" ? $this->passed() : $this->failed(__FUNCTION__);
    }

    public function testInvalidInputAlphabet() {

        $states = ["S0", "S1", "S2"];
        $inputAlphabet = ["1", "0"];

        $initialState = "S0";

        $validFinalStates = ["S0", "S1", "S2"];
        $transitions = [
            "S0" => [ "0" => "S0", "1" => "S1", ],
            "S1" => [ "0" => "S2", "1" => "S0", ],
            "S2" => [ "0" => "S1", "1" => "S2", ]
        ];

        $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);

        // invalid
        $input = "11012";

        try {
            for ($i = 0; $i < strlen($input); $i++) {
                $stateMachine->transition($input[$i]);
            }
        } catch (InvalidArgumentException $e) {
            $this->passed();
            return;
        }

        $this->failed(__FUNCTION__);

    }

    public function testInvalidInitialState() {

        $states = ["S0", "S1", "S2"];
        $inputAlphabet = ["1", "0"];

        // invalid
        $initialState = "S10000";

        $validFinalStates = ["S0", "S1", "S2"];
        $transitions = [
            "S0" => [ "0" => "S0", "1" => "S1", ],
            "S1" => [ "0" => "S2", "1" => "S0", ],
            "S2" => [ "0" => "S1", "1" => "S2", ]
        ];

        try {
            $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);
        } catch (InvalidArgumentException $e) {
            $this->passed();
            return;
        }

        $this->failed(__FUNCTION__);

    }

    public function testInvalidFinalStates() {

        $states = ["S0", "S1", "S2"];
        $inputAlphabet = ["1", "0"];

        $initialState = "S1";

        // invalid
        $validFinalStates = ["S0", "S1", "S1000"];

        $transitions = [
            "S0" => [ "0" => "S0", "1" => "S1", ],
            "S1" => [ "0" => "S2", "1" => "S0", ],
            "S2" => [ "0" => "S1", "1" => "S2", ]
        ];

        try {
            $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);
        } catch (InvalidArgumentException $e) {
            $this->passed();
            return;
        }

        $this->failed(__FUNCTION__);
    }

    
    public function testInvalidTransitionStructState() {

        $states = ["S0", "S1", "S2"];
        $inputAlphabet = ["1", "0"];

        $initialState = "S1";

        $validFinalStates = ["S0", "S1", "S2"];

        $transitions = [
            // invalid
            "124" => [ "0" => "S0", "1" => "S1", ],

            "S1" => [ "0" => "S2", "1" => "S0", ],
            "S2" => [ "0" => "S1", "1" => "S2", ]
        ];

        try {
            $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);
        } catch (InvalidArgumentException $e) {
            $this->passed();
            return;
        }

        $this->failed(__FUNCTION__);
    }

    public function testInvalidTransitionStructAlphabet() {
        $states = ["S0", "S1", "S2"];
        $inputAlphabet = ["1", "0"];

        $initialState = "S1";

        $validFinalStates = ["S0", "S1", "S2"];

        $transitions = [
            "S0" => [ "0" => "S0", "1" => "S1", ],
                    // A invalid
            "S1" => [ "A" => "S2", "1" => "S0", ],
            "S2" => [ "0" => "S1", "1" => "S2", ]
        ];

        try {
            $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);
        } catch (InvalidArgumentException $e) {
            $this->passed();
            return;
        }

        $this->failed(__FUNCTION__);
    }

    public function testInvalidTransitionStructAlphabetTransitionState() {
        $states = ["S0", "S1", "S2"];
        $inputAlphabet = ["1", "0"];

        $initialState = "S1";

        $validFinalStates = ["S0", "S1", "S2"];

        $transitions = [
            "S0" => [ "0" => "S0", "1" => "S1", ],
            "S1" => [ "0" => "S2", "1" => "S0", ],
                                          // invalid
            "S2" => [ "0" => "S1", "1" => 3500]
        ];

        try {
            $stateMachine = new StateMachine($states, $inputAlphabet, $initialState, $validFinalStates, $transitions);
        } catch (InvalidArgumentException $e) {
            $this->passed();
            return;
        }

        $this->failed(__FUNCTION__);
    }

    private function passed() { $this->passed++; }

    private function failed($functionName) { 
        $this->failed[] = $functionName;

    }

}


